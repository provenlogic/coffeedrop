package com.nikola.user.Adapter;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.nikola.user.AdapterCallback;
import com.nikola.user.Models.TaxiTypes;
import com.nikola.user.R;
import com.nikola.user.SubtypeActivity;
import com.nikola.user.TypeSizeActivity;

import java.text.DecimalFormat;
import java.util.List;

/**
 * Created by user on 10/5/2016.
 */
public class TaxiAdapter extends RecyclerView.Adapter<TaxiAdapter.typesViewHolder> {

    private Activity mContext;
    private AdapterCallback mAdapterCallback;
    private List<TaxiTypes> taxiTypesList;
    public int pos;
    DecimalFormat format = new DecimalFormat("0.00");

    public TaxiAdapter(Activity context, List<TaxiTypes> taxiTypesList, AdapterCallback mAdapterCallback) {
        mContext = context;
        this.taxiTypesList = taxiTypesList;
        this.mAdapterCallback = mAdapterCallback;

    }

    @Override
    public TaxiAdapter.typesViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.vehicle_type_item,parent,false);
        typesViewHolder holder = new typesViewHolder(view);

        return holder;
    }



    @Override
    public void onBindViewHolder(final TaxiAdapter.typesViewHolder holder, int position) {
        final TaxiTypes list_types = taxiTypesList.get(position);
//        holder.tv_type_name.setText(list_types.getTaxitype());
        holder.tv_type_name.setText("Coffee");

        if (null!=list_types.getTaxi_cost()&&!list_types.getTaxi_cost().equals("")) {
            holder.tv_type_cost.setText(list_types.getCurrencey_unit()+format.format(Double.valueOf(list_types.getTaxi_cost())));
        } else {
            holder.tv_type_cost.setText(list_types.getCurrencey_unit()+list_types.getTaxi_cost());
        }

//        Glide.with(mContext).load(list_types.getTaxiimage()).error(R.drawable.frontal_taxi_cab).into(holder.type_picutre);

//        holder.tv_type_cost.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                mAdapterCallback.onMethodCallback(list_types.getId(), list_types.getTaxitype(), list_types.getTaxi_price_distance(), list_types.getTaxi_price_min(), list_types.getTaxiimage(), list_types.getTaxi_seats(), list_types.getBasefare());
//            }
//        });
     /*
        holder.type_picutre.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                holder.type_picutre.setBorderColor(mContext.getResources().getColor(R.color.ripple_blue));
            }
        });*/

        if (pos == position) {
            holder.view_select.setVisibility(View.VISIBLE);
        } else {
            holder.view_select.setVisibility(View.INVISIBLE);
        }

        holder.foodType.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, TypeSizeActivity.class);
                mContext.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return taxiTypesList.size();
    }

    public class typesViewHolder extends RecyclerView.ViewHolder {
        private ImageView type_picutre;
        private TextView tv_type_name, tv_type_cost;
        private View view_select;
        private LinearLayout foodType;

        public typesViewHolder(View itemView) {
            super(itemView);
            type_picutre = (ImageView) itemView.findViewById(R.id.type_picutre);
            tv_type_name = (TextView) itemView.findViewById(R.id.tv_type_name);
            tv_type_cost = (TextView) itemView.findViewById(R.id.tv_type_cost);
            view_select = (View)itemView.findViewById(R.id.view_select);
            foodType = (LinearLayout) itemView.findViewById(R.id.food_type);

        }
    }

    public void OnItemClicked(int position) {
        pos = position;
        //Log.d("mahi", "pos" + pos);
    }
}
