package com.nikola.user;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.nikola.user.Adapter.AdOnsAdapter;
import com.nikola.user.Adapter.SubtypesAdapter;
import com.nikola.user.Models.SubtypeItems;

import java.util.ArrayList;

/**
 * Created by Mukesh on 20-12-2017.
 */

public class SubtypeActivity extends AppCompatActivity {

    private RecyclerView subTypes;
    private SubtypesAdapter adapter;
    private SubtypeItems item, item2, item3, item4, item5;
    private ArrayList<SubtypeItems> itemList;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_subtypes);
        subTypes = (RecyclerView) findViewById(R.id.subtypes);

        item = new SubtypeItems();
        item2 = new SubtypeItems();
        item3 = new SubtypeItems();
        item4 = new SubtypeItems();
        item5 = new SubtypeItems();

        item.setI_name("Espresso Shot");
        item2.setI_name("Double Espresso");
        item3.setI_name("Latte");
        item4.setI_name("Short Macchiato");
        item5.setI_name("Americano");

        itemList = new ArrayList<>();
        itemList.add(0, item);
        itemList.add(1, item2);
        itemList.add(2, item3);
        itemList.add(3, item4);
        itemList.add(4, item5);

//        for(int i = 0 ; i < 20 ; ++i){
//            SubtypeItems type = new SubtypeItems();
//            type.setI_name("Coffee " +(i+1));
//            itemList.add(type);
//        }

        adapter = new SubtypesAdapter(this, itemList);
        subTypes.setLayoutManager(new LinearLayoutManager(this));
        subTypes.setItemAnimator(new DefaultItemAnimator());
        subTypes.setAdapter(adapter);

        /**
         * For AdOns,
         *
         */
//        adons = new ArrayList<>();
//
//        adons.add(0, "Milk");
//        adons.add(1, "Sugar");
//        adons.add(2, "Chocolate powder");
//
//        adOnsAdapter = new AdOnsAdapter(this, adons);
//        adOns = (RecyclerView) findViewById(R.id.ad_ons_recycler_view);
//        adOns.setLayoutManager(new LinearLayoutManager(this));
//        adOns.setItemAnimator(new DefaultItemAnimator());
//        adOns.setAdapter(adOnsAdapter);
    }
}
