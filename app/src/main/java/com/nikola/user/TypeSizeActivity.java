package com.nikola.user;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.provider.CalendarContract;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

/**
 * Created by Mukesh on 21-12-2017.
 */

public class TypeSizeActivity extends AppCompatActivity {
    private TextView typeName, smallText, standardText, largeText;
    private ImageView typeImg;
    private LinearLayout small, standard, large, customize;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_type_size);

        smallText = (TextView) findViewById(R.id.small_txt);
        standardText = (TextView) findViewById(R.id.standard_txt);
        largeText = (TextView) findViewById(R.id.large_txt);

        customize = (LinearLayout) findViewById(R.id.customize_btn);
        small = (LinearLayout) findViewById(R.id.small);
        standard = (LinearLayout) findViewById(R.id.standard);
        large = (LinearLayout) findViewById(R.id.large);

        small.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                small.setBackgroundResource(R.color.red_coffee);
                smallText.setTextColor(getResources().getColor(R.color.white));

                standard.setBackgroundResource(R.color.white);
                standardText.setTextColor(getResources().getColor(R.color.red_coffee));

                large.setBackgroundResource(R.color.white);
                largeText.setTextColor(getResources().getColor(R.color.red_coffee));
            }
        });

        standard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                small.setBackgroundResource(R.color.white);
                smallText.setTextColor(getResources().getColor(R.color.red_coffee));

                standard.setBackgroundResource(R.color.red_coffee);
                standardText.setTextColor(getResources().getColor(R.color.white));

                large.setBackgroundResource(R.color.white);
                largeText.setTextColor(getResources().getColor(R.color.red_coffee));
            }
        });

        large.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                small.setBackgroundResource(R.color.white);
                smallText.setTextColor(getResources().getColor(R.color.red_coffee));

                standard.setBackgroundResource(R.color.white);
                standardText.setTextColor(getResources().getColor(R.color.red_coffee));

                large.setBackgroundResource(R.color.red_coffee);
                largeText.setTextColor(getResources().getColor(R.color.white));
            }
        });

        customize.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(TypeSizeActivity.this, SubtypeActivity.class);
                startActivity(intent);
            }
        });
    }
}
