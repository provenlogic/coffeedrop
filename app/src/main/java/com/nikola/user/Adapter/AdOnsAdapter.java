package com.nikola.user.Adapter;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;

import com.nikola.user.R;

import java.util.ArrayList;

/**
 * Created by Mukesh on 26-12-2017.
 */

public class AdOnsAdapter extends RecyclerView.Adapter<AdOnsAdapter.OptionsHolder> {
    private Activity activity;
    private ArrayList<String> adons;

    public AdOnsAdapter(Activity activity, ArrayList<String> adons) {
        this.activity = activity;
        this.adons = adons;
    }

    @Override
    public OptionsHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(activity).inflate(R.layout.list_item_ad_ons, parent, false);
        return new OptionsHolder(v);
    }

    @Override
    public void onBindViewHolder(OptionsHolder holder, int position) {
        holder.adons_checkBox.setText(adons.get(position));
    }

    @Override
    public int getItemCount() {
        return adons.size();
    }

    class OptionsHolder extends RecyclerView.ViewHolder {
        CheckBox adons_checkBox;

        public OptionsHolder(View view) {
            super(view);
            adons_checkBox = (CheckBox) view.findViewById(R.id.ad_ons);
        }
    }
}
