package com.nikola.user.Models;

/**
 * Created by Mukesh on 20-12-2017.
 */

public class SubtypeItems {
    String i_name, i_id, i_icon;
    int i_quantity;

    public String getI_name() {
        return i_name;
    }

    public void setI_name(String i_name) {
        this.i_name = i_name;
    }

    public String getI_id() {
        return i_id;
    }

    public void setI_id(String i_id) {
        this.i_id = i_id;
    }

    public String getI_icon() {
        return i_icon;
    }

    public void setI_icon(String i_icon) {
        this.i_icon = i_icon;
    }

    public int getI_quantity() {
        return i_quantity;
    }

    public void setI_quantity(int i_quantity) {
        this.i_quantity = i_quantity;
    }
}
