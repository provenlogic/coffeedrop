package com.nikola.user.Adapter;

import android.app.Activity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.nikola.user.Models.SubtypeItems;
import com.nikola.user.R;

import net.cachapa.expandablelayout.ExpandableLayout;

import java.util.ArrayList;

import me.himanshusoni.quantityview.QuantityView;

/**
 * Created by Mukesh on 20-12-2017.
 */

public class SubtypesAdapter extends RecyclerView.Adapter<SubtypesAdapter.OptionsHolder> {

    private Activity activity;
    private ArrayList<SubtypeItems> itemList;
    private AdOnsAdapter adOnsAdapter;
    private ArrayList<String> adonsItems;

    public SubtypesAdapter(Activity activity, ArrayList<SubtypeItems> itemList) {
        this.activity = activity;
        this.itemList = itemList;
    }

    @Override
    public OptionsHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(activity).inflate(R.layout.list_item_subtypes, parent, false);
        return new OptionsHolder(v);
    }

    @Override
    public void onBindViewHolder(final OptionsHolder holder, int i) {
        holder.i_name.setText(itemList.get(i).getI_name());
        holder.i_img.setBackgroundResource(R.drawable.coffee_img);
        holder.i_quantity.getQuantity();

        adonsItems = new ArrayList<>();
        adonsItems.add(0, "Milk");
        adonsItems.add(1, "Sugar");
        adonsItems.add(2, "Chocolate powder");
        adOnsAdapter = new AdOnsAdapter(activity, adonsItems);
        holder.adOnsRecyclerView.setLayoutManager(new LinearLayoutManager(activity));
        holder.adOnsRecyclerView.setItemAnimator(new DefaultItemAnimator());
        holder.adOnsRecyclerView.setAdapter(adOnsAdapter);

        holder.mainRelative.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (holder.expandableLayout.isExpanded()){
                    holder.expandableLayout.collapse();
                }else {
                    holder.expandableLayout.expand();
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return itemList.size();
    }

    class OptionsHolder extends RecyclerView.ViewHolder {
        TextView i_name;
        ImageView i_img;
        QuantityView i_quantity;
        RelativeLayout mainRelative;
        ExpandableLayout expandableLayout;
        RecyclerView adOnsRecyclerView;

        public OptionsHolder(View view) {
            super(view);
            i_name = (TextView) view.findViewById(R.id.i_name);
            i_img = (ImageView) view.findViewById(R.id.i_img);
            i_quantity = (QuantityView) view.findViewById(R.id.i_quantity);
            mainRelative = (RelativeLayout) view.findViewById(R.id.main_relative);
            expandableLayout = (ExpandableLayout) view.findViewById(R.id.expandable_adons);
            adOnsRecyclerView = (RecyclerView) view.findViewById(R.id.ad_ons_recycler_view);
        }
    }
}
